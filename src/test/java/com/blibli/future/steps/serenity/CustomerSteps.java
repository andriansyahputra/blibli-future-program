package com.blibli.future.steps.serenity;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.openqa.selenium.WebDriver;
import com.blibli.future.pages.*;
import org.openqa.selenium.WebElement;

import java.util.List;

import static com.blibli.future.utils.Utility.getConvertedMonth;

import static org.assertj.core.api.Assertions.assertThat;


public class CustomerSteps extends ScenarioSteps {
    String actor;

    HomePage homePage;
    FlightLandingPage flightLandingPage;
    FlightSearchPage flightSearchPage;
    FlightCheckoutPage flightCheckoutPage;
    FlightPaymentPage flightPaymentPage;

    @Step("#actor opens home page")
    public void opens_home_page() {
        homePage.open();
    }

    @Step("#actor clicks 'airplane' icon in 'favorites' tab to go to flight landing page")
    public void go_to_flight_landing_page_diretly(){
        homePage.openFlightDirectlyFromTravelIconOnHomePage();
    }

    @Step("#actor goes to 'Favorites' and go to flight landing page from there")
    public void go_to_flight_landing_page_from_favorites(){
        homePage.openFlightFromFavorites();
    }

    @Step("#actor should arrives at the flight landing page")
    public void should_arrive_in_flight_landing_page(){
        assertThat(flightLandingPage.getHeaderText()).containsIgnoringCase("pesan tiket pesawat");
    }

    @Step("#actor opens flight landing page from URL")
    public void opens_flight_landing_page() {
        flightLandingPage.open();
    }

    @Step("#actor search tickets with pre-filled data (empty data from #actor)")
    public void search_ticket_vanilla_way(){
        flightLandingPage.searchTicket();
    }

    @Step("#actor fills departure or arrival airport, #actor should see assisting auto-complete recommendations")
    public void get_auto_complete_airports(String type, String location){
        assertThat(flightLandingPage.searchAirportsAutoComplete(type, location));
    }

    @Step("#actor fills departure or arrival airport that is not present in database")
    public void fills_invalid_airport(String type, String location){
        assertThat(flightLandingPage.fillDomesticAirportsNotPresent(type, location));
    }

    @Step("#actor cannot choose the day before today (yesterday) as the date of flight")
    public void choose_day_before_today(){
        assertThat(flightLandingPage.disabledCalendarDayBeforeToday());
    }

    @Step("#actor fills the date of flight")
    public void fills_date_of_flight(WebDriver driver, int date, int month, int year){
        flightLandingPage.chooseDateOfFlight(driver, date, month, year);
    }

    @Step("#actor cannot choose return date before depart date in roundtrip flights")
    public void get_return_date_roundtrip(WebDriver driver){
        assertThat(flightLandingPage.cannotReturnBeforeDepartDate(driver));
    }

    @Step("#actor fills return date for roundtrip flights")
    public void fill_return_date_roundtrip(WebDriver driver, int date, int month, int year){
        flightLandingPage.chooseReturnDateRoundtrip(driver, date, month, year);
    }

    @Step("#actor fills total of passengers")
    public void fill_amount_of_passengers(WebDriver driver, int adult, int teen, int infant){
        flightLandingPage.fillPassengerTotal(driver, adult, teen, infant);
    }

    @Step("#actor fills type of flight class")
    public void fill_class_type(WebDriver driver, String type){
        flightLandingPage.fillFlightClassType(driver, type);
    }

    @Step("#actor should see roundtrip return date field disabled if not going on roundtrip flights")
    public void disabled_roundtrip_date_return(){
        assertThat(flightLandingPage.dateFieldDisabledIfNotRoundtrip());
    }

    @Step("#actor clicks search button")
    public void click_button(){
        flightLandingPage.searchTicket();
    }

    @Step("#actor fills international's arrival and departure airport")
    public void fills_international_airport(String depart, String arrive){
        flightLandingPage.fillAirports(depart, arrive);
    }

    @Step("#actor fills domestic's arrival and departure airport")
    public void fills_domestic_airport(String depart, String arrive){
        flightLandingPage.fillAirports(depart, arrive);
    }

    @Step("#actor should see tickets search result")
    public void should_arrive_in_ticket_search_result_page(String departureAirport, String arrivalAirport,
                                                           int departDate, int departMonth, int departYear,
                                                           boolean isRoundtrip, int returnDate, int returnMonth,
                                                           int returnYear, int adult, int teen, int infant,
                                                           String classType){
        assertThat(flightSearchPage.getDepartureAirportText()).containsIgnoringCase(departureAirport);

        assertThat(flightSearchPage.getArrivalAirportText()).containsIgnoringCase(arrivalAirport);

        List<WebElement> dates = flightSearchPage.getDepartAndArriveDate();
        String departedDate = dates.get(0).getText();
        assertThat(departedDate.contains(String.valueOf(departDate))
                && departedDate.contains(getConvertedMonth(String.valueOf(departMonth)))
                && departedDate.contains(String.valueOf(departYear)));
        if (isRoundtrip) {
            String returnedDate = dates.get(dates.size()-1).getText();
            assertThat(returnedDate.contains(String.valueOf(returnDate))
                    && returnedDate.contains(getConvertedMonth(String.valueOf(returnMonth)))
                    && returnedDate.contains(String.valueOf(returnYear)));
        }

        String[] passengerInfoSplited = flightSearchPage.getPassengersInfoText().split(" ");
        if (adult > 0) assertThat(passengerInfoSplited[0].equalsIgnoreCase(String.valueOf(adult)));
        if (teen == 0) {
            if (infant > 0) assertThat(passengerInfoSplited[2].equalsIgnoreCase(String.valueOf(infant)));

        } else {
            assertThat(passengerInfoSplited[2].equalsIgnoreCase(String.valueOf(teen)));
            if (infant > 0) assertThat(passengerInfoSplited[4].equalsIgnoreCase(String.valueOf(infant)));
        }

        assertThat(flightSearchPage.getFlightClassText()).containsIgnoringCase(classType);
    }

    @Step("#actor should arrives in ticket search page")
    public void should_arrive_in_ticket_search_page_simple(){
        assertThat(flightSearchPage.getTextContainer().contains("Pencarian Pergi"));
    }

    @Step("#actor fills airport")
    public void fills_airports(String depart, String arrive){
        flightLandingPage.fillAirports(depart, arrive);
    }

    @Step("#actor clicks swap airport button")
    public void swap_airport(WebDriver driver){
        flightLandingPage.clickSwapAirportsButton(driver);
    }

    @Step("#actor should see that the airports are getting swapped")
    public void should_see_airports_getting_swapped(String depart, String arrive){
        assertThat(flightSearchPage.getDepartureAirportText().contains(arrive));
        assertThat(flightSearchPage.getArrivalAirportText().contains(depart));
    }

    @Step("#actor filter prices with max value from slider")
    public void price_slider_max(WebDriver driver){
        assertThat(flightSearchPage.priceSliderFilterToMax(driver) == flightSearchPage.getPriceFilter(1));
    }

    @Step("#actor sort by departure time ascending")
    public void sort_depart_time_asc(WebDriver driver){
        flightSearchPage.sortByDepartureTime(driver, "ascending");
    }

    @Step("#actor sort by departure time descending")
    public void sort_depart_time_desc(WebDriver driver){
        flightSearchPage.sortByDepartureTime(driver, "descending");
    }

    @Step("#actor sort by arrival time ascending")
    public void sort_arrival_time_asc(WebDriver driver){
        flightSearchPage.sortByArriveTime(driver, "ascending");
    }

    @Step("#actor sort by arrival time descending")
    public void sort_arrival_time_desc(WebDriver driver){
        flightSearchPage.sortByArriveTime(driver, "descending");
    }

    @Step("#actor sort by price ascending")
    public void sort_price_asc(WebDriver driver){
        flightSearchPage.sortByPrice(driver, "ascending");
    }

    @Step("#actor sort by price descending")
    public void sort_price_desc(WebDriver driver){
        flightSearchPage.sortByPrice(driver, "descending");
    }

    @Step("#actor sort by duration ascending")
    public void sort_duration_asc(WebDriver driver){
        flightSearchPage.sortByDuration(driver, "ascending");
    }

    @Step("#actor sort by duration descending")
    public void sort_duration_desc(WebDriver driver){
        flightSearchPage.sortByDuration(driver, "descending");
    }

    @Step("#actor wants to check payment details")
    public void assert_payment_details(String[] data, WebDriver driver){
        assertThat(data[0].contains(flightPaymentPage.getAirline(driver)));
        assertThat(data[1].contains(flightPaymentPage.getFlightClass(driver)));
        assertThat(data[2].contains(flightPaymentPage.getKotaKeberangkatanDanKedatanganText(driver)));
        assertThat(data[3].equalsIgnoreCase(flightPaymentPage.getNominalHarga(driver)));
    }

    @Step("#actor choose one ticket that first appear on top of the list")
    public void choose_first_ticket(){
        flightSearchPage.pickFirstOccurrenceTicket();
    }

    @Step("#actor should go to checkout page")
    public void should_arrive_in_ticket_checkout_page(){
        flightCheckoutPage.lookUpLandingPageText();
        assertThat(flightCheckoutPage.getLandingPageTextContainer()).containsIgnoringCase("data penumpang");
    }

    @Step("#actor fills 'Kontak Pemesan' data")
    public void fill_kontak_pemesan_fields(WebDriver webDriver){
        flightCheckoutPage.fillKontakPemesan(webDriver);
    }

    @Step("#actor fills data and info necessary for domestic flight")
    public void fill_domestic_info_data(WebDriver webDriver){
        flightCheckoutPage.fillIdentityDomestic(webDriver);
    }

    @Step("#actor fills data and info necessary for international flight")
    public void fill_international_info_data(WebDriver webDriver){
        flightCheckoutPage.fillIdentityInternational(webDriver);
    }

    @Step("#actor submit data by clicking button")
    public void submit_data(){
        flightCheckoutPage.submitDataButtonClick();
    }

    @Step("#actor submit data by clicking button to go to checkout page")
    public void next_submit_data(WebDriver driver){
        flightCheckoutPage.nextSubmitDataButtonClick(driver);
    }

    @Step("#actor retrieve data to be checked")
    public String[] data_retrieval(){
        String[] data = new String[4];
        data[0] = flightCheckoutPage.getAirline();
        data[1] = flightCheckoutPage.getFlightClass();
        data[2] = flightCheckoutPage.getDestinations();
        data[3] = flightCheckoutPage.getPrice();
        return data;
    }

    @Step("#actor should receive summary of data that just being filled")
    public void should_get_data_summary(){
        flightCheckoutPage.lookUpDataSubmitText();
        assertThat(flightCheckoutPage.getDataSubmitTextContainer()).containsIgnoringCase("pastikan seluruh data Anda");
    }
}
