package com.blibli.future.utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

public class Utility {
    final static HashMap<String, String> months = new HashMap<String, String>()
    {{
        put("0", "Januari");
        put("1", "Februari");
        put("2", "Maret");
        put("3", "April");
        put("4", "Mei");
        put("5", "Juni");
        put("6", "Juli");
        put("7", "Agustus");
        put("8", "September");
        put("9", "Oktober");
        put("10", "November");
        put("10", "Desember");
    }};


    public static Properties getPropertiesFile(String file) throws IOException {
        InputStream rootPath = Utility.class.getClassLoader().getResourceAsStream(file);
        Properties dataProp = new Properties();
        dataProp.load(rootPath);
        return dataProp;
    }

    public static String getConvertedMonth(String key){
        return months.get(key);
    }
}
