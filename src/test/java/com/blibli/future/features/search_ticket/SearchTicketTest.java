package com.blibli.future.features.search_ticket;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import com.blibli.future.steps.serenity.CustomerSteps;
import java.io.IOException;
import static com.blibli.future.utils.Utility.*;

@RunWith(SerenityRunner.class)
@Narrative(text={"Search flight tickets for passengers."})
public class SearchTicketTest {
    @Managed
    WebDriver webDriver;

    @Steps
    CustomerSteps customer;

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="test auto-complete for airports")
    })
    public void airport_auto_complete() throws IOException {
        customer.opens_flight_landing_page();
        String departureAirport = getPropertiesFile("data.properties").getProperty("valid.airport.domestic.departure");
        customer.get_auto_complete_airports("depart", departureAirport);
    }

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="test auto-complete for not found airports")
    })
    public void not_found_airports() throws IOException {
        customer.opens_flight_landing_page();
        String departureAirport = getPropertiesFile("data.properties").getProperty("invalid.airport.departure");
        customer.fills_invalid_airport("depart", departureAirport);
    }

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="test if the day before today is disabled")
    })
    public void day_before_today_disabled() {
        customer.opens_flight_landing_page();
        customer.choose_day_before_today();
    }

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="fills the date of flight")
    })
    public void choose_flight_date() throws IOException {
        customer.opens_flight_landing_page();
        int departDate = Integer.parseInt(getPropertiesFile("data.properties").getProperty("departure.date"));
        int departMonth = Integer.parseInt(getPropertiesFile("data.properties").getProperty("departure.month"));
        int departYear = Integer.parseInt(getPropertiesFile("data.properties").getProperty("departure.year"));
        customer.fills_date_of_flight(webDriver, departDate, departMonth, departYear);
    }

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="roundtrip return date field should be disabled if not going on roundtrip flights")
    })
    public void roundtrip_date_disabled(){
        customer.opens_flight_landing_page();
        customer.disabled_roundtrip_date_return();
    }

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="cannot choose return date before depart date")
    })
    public void get_return_date_round_trip(){
        customer.opens_flight_landing_page();
        customer.get_return_date_roundtrip(webDriver);
    }

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="swap airport button check")
    })
    public void check_swap_airport_button() throws IOException {
        customer.opens_flight_landing_page();

        String departureAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.departure");
        String arrivalAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.arrival");

        customer.fills_airports(departureAirport, arrivalAirport);

        customer.swap_airport(webDriver);

        customer.click_button();

        customer.should_see_airports_getting_swapped(departureAirport, arrivalAirport);
    }

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="search ticket for international flights")

    })
    public void regression_international_ticket() throws IOException {
        customer.opens_flight_landing_page();

        String departureAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.departure");
        String arrivalAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.arrival");

        int departDate = Integer.parseInt(getPropertiesFile("data.properties").getProperty("departure.date"));
        int departMonth = Integer.parseInt(getPropertiesFile("data.properties").getProperty("departure.month"));
        int departYear = Integer.parseInt(getPropertiesFile("data.properties").getProperty("departure.year"));

        boolean isRoundtrip = Boolean.parseBoolean(getPropertiesFile("data.properties").getProperty("roundtrip"));

        int returnDate = Integer.parseInt(getPropertiesFile("data.properties").getProperty("return.date"));
        int returnMonth = Integer.parseInt(getPropertiesFile("data.properties").getProperty("return.month"));
        int returnYear = Integer.parseInt(getPropertiesFile("data.properties").getProperty("return.year"));

        int adult = Integer.parseInt(getPropertiesFile("data.properties").getProperty("adult"));
        int teen = Integer.parseInt(getPropertiesFile("data.properties").getProperty("teen"));
        int infant = Integer.parseInt(getPropertiesFile("data.properties").getProperty("infant"));

        String classType = getPropertiesFile("data.properties").getProperty("flight.class");

        customer.fills_international_airport(departureAirport, arrivalAirport);

        customer.fills_date_of_flight(webDriver, departDate, departMonth, departYear);

        if(isRoundtrip) customer.fill_return_date_roundtrip(webDriver, returnDate, returnMonth, returnYear);

        customer.fill_amount_of_passengers(webDriver, adult, teen, infant);

        customer.fill_class_type(webDriver, classType);

        customer.click_button();

        customer.should_arrive_in_ticket_search_result_page(departureAirport,arrivalAirport, departDate, departMonth, departYear,
                isRoundtrip, returnDate, returnMonth, returnYear, adult, teen, infant, classType);
    }
}
