package com.blibli.future.features.open_flight_page;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import com.blibli.future.steps.serenity.CustomerSteps;

@RunWith(SerenityRunner.class)
@Narrative(text={"Open Blibli.com's flight landing page from home page"})
public class WhenOpenFlightLandingPage {
    @Managed
    WebDriver webDriver;

    @Steps
    CustomerSteps customer;

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="open flight landing page directly")

    })
    public void open_flight_landing_page_directly() {
        customer.opens_home_page();
        customer.go_to_flight_landing_page_diretly();
        customer.should_arrive_in_flight_landing_page();
    }

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="open flight landing page from 'Favorites'")

    })
    public void open_flight_landing_page_from_favorites() {
        customer.opens_home_page();
        customer.go_to_flight_landing_page_from_favorites();
        customer.should_arrive_in_flight_landing_page();
    }
}
