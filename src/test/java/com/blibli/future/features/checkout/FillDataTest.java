package com.blibli.future.features.checkout;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import com.blibli.future.features.search_ticket.SearchTicketTest;
import com.blibli.future.steps.serenity.CustomerSteps;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import static com.blibli.future.utils.Utility.getPropertiesFile;;

@RunWith(SerenityRunner.class)
@Narrative(text={"Filling necessary data"})
public class FillDataTest {
    @Managed
    WebDriver webDriver;

    @Steps
    CustomerSteps customer;

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="filling data and info needed for domestic flight")

    })
    public void fill_data_domestic_flight() throws IOException {
        customer.opens_flight_landing_page();

        String departureAirport = getPropertiesFile("data.properties").getProperty("valid.airport.domestic.departure");
        String arrivalAirport = getPropertiesFile("data.properties").getProperty("valid.airport.domestic.arrival");

        customer.fills_domestic_airport(departureAirport, arrivalAirport);

        customer.click_button();

        customer.should_arrive_in_ticket_search_page_simple();

        customer.choose_first_ticket();

        customer.should_arrive_in_ticket_checkout_page();

        customer.fill_kontak_pemesan_fields(webDriver);

        customer.fill_domestic_info_data(webDriver);

        customer.submit_data();

        customer.should_get_data_summary();
    }

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="filling data and info needed for international flight")

    })
    public void fill_data_international_flight() throws IOException {
        customer.opens_flight_landing_page();

        String departureAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.departure");
        String arrivalAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.arrival");

        customer.fills_international_airport(departureAirport, arrivalAirport);

        customer.click_button();

        customer.should_arrive_in_ticket_search_page_simple();

        customer.choose_first_ticket();

        customer.should_arrive_in_ticket_checkout_page();

        customer.fill_kontak_pemesan_fields(webDriver);

        customer.fill_international_info_data(webDriver);

        customer.submit_data();

        customer.should_get_data_summary();
    }
}
