package com.blibli.future.features.payment;

import com.blibli.future.steps.serenity.CustomerSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

import static com.blibli.future.utils.Utility.getPropertiesFile;

@RunWith(SerenityRunner.class)
@Narrative(text={"Payment for ticket"})
public class WhenPaymentTest {
    @Managed
    WebDriver webDriver;

    @Steps
    CustomerSteps customer;

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="filling data and info needed for international flight")

    })
    public void flight_details() throws IOException {
        customer.opens_flight_landing_page();

        String departureAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.departure");
        String arrivalAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.arrival");

        customer.fills_international_airport(departureAirport, arrivalAirport);

        customer.click_button();

        customer.should_arrive_in_ticket_search_page_simple();

        customer.choose_first_ticket();

        customer.should_arrive_in_ticket_checkout_page();

        customer.fill_kontak_pemesan_fields(webDriver);

        customer.fill_international_info_data(webDriver);

        String[] data = customer.data_retrieval();

        customer.submit_data();

        customer.should_get_data_summary();

        customer.next_submit_data(webDriver);

        customer.assert_payment_details(data, webDriver);
    }
}
