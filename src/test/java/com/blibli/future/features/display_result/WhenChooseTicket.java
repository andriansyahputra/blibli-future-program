package com.blibli.future.features.display_result;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import com.blibli.future.features.search_ticket.SearchTicketTest;
import com.blibli.future.steps.serenity.CustomerSteps;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import static com.blibli.future.utils.Utility.getPropertiesFile;

@RunWith(SerenityRunner.class)
@Narrative(text={"Choosing ticket from available options"})
public class WhenChooseTicket {
    @Managed
    WebDriver webDriver;

    @Steps
    CustomerSteps customer;

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="choose first occurrence ticket")

    })
    public void choose_first_ticket() throws IOException {
        customer.opens_flight_landing_page();

        String departureAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.departure");
        String arrivalAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.arrival");

        customer.fills_airports(departureAirport, arrivalAirport);

        customer.click_button();

        customer.should_arrive_in_ticket_search_page_simple();

        customer.choose_first_ticket();

        customer.should_arrive_in_ticket_checkout_page();
    }

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="choose first occurrence ticket")

    })
    public void max_price_filter_slider_should_return_max_price() throws IOException {
        customer.opens_flight_landing_page();

        String departureAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.departure");
        String arrivalAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.arrival");

        customer.fills_airports(departureAirport, arrivalAirport);

        customer.click_button();

        customer.should_arrive_in_ticket_search_page_simple();

        customer.price_slider_max(webDriver);
    }

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="sort by departure time, ascending")

    })
    public void can_sort_by_depart_time_asc() throws IOException {
        customer.opens_flight_landing_page();

        String departureAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.departure");
        String arrivalAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.arrival");

        customer.fills_airports(departureAirport, arrivalAirport);

        customer.click_button();

        customer.should_arrive_in_ticket_search_page_simple();

        customer.sort_depart_time_asc(webDriver);
    }

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="sort by departure time, descending")

    })
    public void can_sort_by_depart_time_desc() throws IOException {
        customer.opens_flight_landing_page();

        String departureAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.departure");
        String arrivalAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.arrival");

        customer.fills_airports(departureAirport, arrivalAirport);

        customer.click_button();

        customer.should_arrive_in_ticket_search_page_simple();

        customer.sort_depart_time_desc(webDriver);
    }

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="sort by arrival time, ascending")

    })
    public void can_sort_by_arrival_time_asc() throws IOException {
        customer.opens_flight_landing_page();

        String departureAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.departure");
        String arrivalAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.arrival");

        customer.fills_airports(departureAirport, arrivalAirport);

        customer.click_button();

        customer.should_arrive_in_ticket_search_page_simple();

        customer.sort_arrival_time_asc(webDriver);
    }

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="sort by arrival time, descending")

    })
    public void can_sort_by_arrival_time_desc() throws IOException {
        customer.opens_flight_landing_page();

        String departureAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.departure");
        String arrivalAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.arrival");

        customer.fills_airports(departureAirport, arrivalAirport);

        customer.click_button();

        customer.should_arrive_in_ticket_search_page_simple();

        customer.sort_arrival_time_desc(webDriver);
    }

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="sort by duration, ascending")

    })
    public void can_sort_by_duration_asc() throws IOException {
        customer.opens_flight_landing_page();

        String departureAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.departure");
        String arrivalAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.arrival");

        customer.fills_airports(departureAirport, arrivalAirport);

        customer.click_button();

        customer.should_arrive_in_ticket_search_page_simple();

        customer.sort_duration_asc(webDriver);
    }

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="sort by duration, descending")

    })
    public void can_sort_by_duration_desc() throws IOException {
        customer.opens_flight_landing_page();

        String departureAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.departure");
        String arrivalAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.arrival");

        customer.fills_airports(departureAirport, arrivalAirport);

        customer.click_button();

        customer.should_arrive_in_ticket_search_page_simple();

        customer.sort_duration_desc(webDriver);
    }

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="sort by price, ascending")

    })
    public void can_sort_by_price_asc() throws IOException {
        customer.opens_flight_landing_page();

        String departureAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.departure");
        String arrivalAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.arrival");

        customer.fills_airports(departureAirport, arrivalAirport);

        customer.click_button();

        customer.should_arrive_in_ticket_search_page_simple();

        customer.sort_price_asc(webDriver);
    }

    @Test
    //@Pending
    @WithTags( {
            @WithTag(type="priority",name="medium"),
            @WithTag(type="component",name="sort by price, descending")

    })
    public void can_sort_by_price_desc() throws IOException {
        customer.opens_flight_landing_page();

        String departureAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.departure");
        String arrivalAirport = getPropertiesFile("data.properties").getProperty("valid.airport.international.arrival");

        customer.fills_airports(departureAirport, arrivalAirport);

        customer.click_button();

        customer.should_arrive_in_ticket_search_page_simple();

        customer.sort_price_desc(webDriver);
    }
}
