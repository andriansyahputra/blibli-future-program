package com.blibli.future.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FlightPaymentPage extends PageObject {
    @FindBy(xpath="//h4[contains(text(),'Pembayaran')]")
    WebElement titleText;

    @FindBy(xpath="//label[contains(text(),'Bayar di Gerai')]")
    WebElement bayarGeraiOption;

    By penyediaLayananDropdown = By.xpath("//span[@class='grid__col-11']");

    @FindBy(xpath="//h4[@class='grid__col-6 text-right']")
    WebElement nomorPesananText;

    @FindBy(xpath="//span[@class='color--orange']//b")
    WebElement nominalHargaText;

    @FindBy(xpath="//li[@class='passenger']")
    WebElement penumpangList;

    @FindBy(xpath="//div[@class='passenger-value__title']")
    WebElement namaPenumpangText;

    @FindBy(xpath="//div[@class='flight-detail-header']//b")
    WebElement kotaKeberangkatanDanKedatanganText;

    @FindBy(xpath="//div[@class='flight-detail-body']//span")
    WebElement tanggalKeberangkatanText;

    @FindBy(xpath="//div[@class='flight-detail-body-bottom']")
    WebElement detailMaskapaiText;

    public String getNominalHarga(WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver, 60);
        return wait.until(ExpectedConditions.visibilityOf(nominalHargaText)).getText();
    }

    public String getKotaKeberangkatanDanKedatanganText(WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver, 60);
        return wait.until(ExpectedConditions.visibilityOf(kotaKeberangkatanDanKedatanganText)).getText();
    }

    public String[] getTanggalKeberangkatanText(){
        return tanggalKeberangkatanText.getText().split("|")[0].split(" ");

    }

    public String getAirline(WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver, 60);
        return wait.until(ExpectedConditions.visibilityOf(kotaKeberangkatanDanKedatanganText)).getText().split("|")[0];
    }

    public String getFlightClass(WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver, 60);
        return wait.until(ExpectedConditions.visibilityOf(kotaKeberangkatanDanKedatanganText)).getText().split("|")[1];
    }

    public String getTitle(WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver, 60);
        return wait.until(ExpectedConditions.visibilityOf(titleText)).getText();
    }
}
