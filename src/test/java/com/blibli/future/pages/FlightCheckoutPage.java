package com.blibli.future.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FlightCheckoutPage extends PageObject {
    @FindBy(xpath="//h4[contains(text(),'Data Penumpang')]")
    WebElement landingPageTextContainer;

    @FindBy(xpath="//strong[contains(text(),'Mohon pastikan seluruh data Anda sudah sesuai')]")
    WebElement dataSubmitTextContainer;

    @FindBy(xpath="//button[@class='button button--orange button--biggest']")
    WebElement dataSubmitButton;

    By honorificDropdown = By.xpath("//select[@name='title']");

    @FindBy(xpath="//input[@name='fullName']")
    WebElement fillName;

    @FindBy(xpath="//input[@name='phoneNumber']")
    WebElement fillPhoneNumber;

    @FindBy(xpath="//input[@name='email']")
    WebElement fillEmail;

    @FindBy(xpath="//label[contains(text(),'Sama dengan pemesan ?')]")
    WebElement copyDataRadioButton;

    By birthdateDropdown = By.xpath("//li[@class='list__items-brd grid__col-3']//select");

    By birthmonthDropdown = By.xpath("//li[@class='list__items-brd grid__col-4 gutter-4pct']//select");

    By birthyearDropdown = By.xpath("//li[@class='list__items-brd grid__col-4']//select");

    @FindBy(xpath="//div[@class='input__wrapper grid__col-6 margin-bottom-15']//input[@class='relative']")
    WebElement fillPassportNumber;

    By publishdateDropdown = By.xpath("//div[7]//div[1]//div[1]//ul[1]//li[1]//select[1]");

    By publishmonthDropdown = By.xpath("//div[7]//div[1]//div[1]//ul[1]//li[2]//select[1]");

    By publishyearDropdown = By.xpath("//div[7]//div[1]//div[1]//ul[1]//li[3]//select[1]");

    By expiredateDropdown = By.xpath("//div[8]//div[1]//div[1]//ul[1]//li[1]//select[1]");

    By expiremonthDropdown = By.xpath("//div[8]//div[1]//div[1]//ul[1]//li[2]//select[1]");

    By expireyearDropdown = By.xpath("//div[8]//div[1]//div[1]//ul[1]//li[3]//select[1]");

    @FindBy(xpath="//div[@class='grid__col-4']//button[@class='button button--orange button--biggest'][contains(text(),'Lanjutkan Pemesanan')]")
    WebElement nextSubmitButton;

    @FindBy(xpath="//b[contains(text(),'Rp')]")
    WebElement priceText;

    @FindBy(xpath="//div[@class='flight-detail-body-bottom']")
    WebElement flightDetails;

    @FindBy(xpath="//div[@class='flight-detail-body']//span")
    WebElement flightInfo;

    @FindBy(xpath="//div[@class='flight-detail-header']//b")
    WebElement destinationText;

    @FindBy(xpath="//div[@class='modal__body-right']//div[@class='font-bold']")
    WebElement nameText;

    @FindBy(xpath="//div[contains(text(),'Pemesanan Anda sedang dikirim ke pihak maskapai, s')]")
    WebElement loaderText;

    public void lookUpLandingPageText() {
        withAction().moveToElement(landingPageTextContainer);
    }

    public void lookUpDataSubmitText() {
        withAction().moveToElement(dataSubmitTextContainer);
    }

    public String getLandingPageTextContainer() {
        return landingPageTextContainer.getText();
    }

    public String getDataSubmitTextContainer() {
        return dataSubmitTextContainer.getText();
    }

    public void fillKontakPemesan(WebDriver webDriver){
        Select findHonorificDropdown = new Select(webDriver.findElement(honorificDropdown));
        findHonorificDropdown.selectByVisibleText("Tuan");

        fillName.sendKeys("hehe");

        fillPhoneNumber.sendKeys("08594389534");

        fillEmail.sendKeys("email@example.com");
    }

    public void fillIdentityDomestic(WebDriver webDriver){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;

        //lookUpElement(copyDataRadioButton);
        js.executeScript("arguments[0].scrollIntoView(true);", landingPageTextContainer);
        js.executeScript("arguments[0].click();",copyDataRadioButton);

        Select selectBirthdateDropdown = new Select(webDriver.findElement(birthdateDropdown));
        selectBirthdateDropdown.selectByVisibleText("22");

        Select selectBirthmonthDropdown = new Select(webDriver.findElement(birthmonthDropdown));
        selectBirthmonthDropdown.selectByVisibleText("Agt");

        Select selectBirthyearDropdown = new Select(webDriver.findElement(birthyearDropdown));
        selectBirthyearDropdown.selectByVisibleText("1999");
    }

    public void fillIdentityInternational(WebDriver webDriver){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;

        js.executeScript("arguments[0].scrollIntoView(true);", landingPageTextContainer);
        js.executeScript("arguments[0].click()",copyDataRadioButton);

        Select selectBirthdateDropdown = new Select(webDriver.findElement(birthdateDropdown));
        selectBirthdateDropdown.selectByVisibleText("22");

        Select selectBirthmonthDropdown = new Select(webDriver.findElement(birthmonthDropdown));
        selectBirthmonthDropdown.selectByVisibleText("Agt");

        Select selectBirthyearDropdown = new Select(webDriver.findElement(birthyearDropdown));
        selectBirthyearDropdown.selectByVisibleText("1999");

        fillPassportNumber.sendKeys("C6728757");

        Select selectPublishdateDropdown = new Select(webDriver.findElement(publishdateDropdown));
        selectPublishdateDropdown.selectByVisibleText("1");

        Select selectPublishmonthDropdown = new Select(webDriver.findElement(publishmonthDropdown));
        selectPublishmonthDropdown.selectByVisibleText("Jan");

        Select selectPublishyearDropdown = new Select(webDriver.findElement(publishyearDropdown));
        selectPublishyearDropdown.selectByVisibleText("2019");

        Select selectExpiredateDropdown = new Select(webDriver.findElement(expiredateDropdown));
        selectExpiredateDropdown.selectByVisibleText("1");

        Select selectExpiremonthDropdown = new Select(webDriver.findElement(expiremonthDropdown));
        selectExpiremonthDropdown.selectByVisibleText("Jan");

        Select selectExpireyearDropdown = new Select(webDriver.findElement(expireyearDropdown));
        selectExpireyearDropdown.selectByVisibleText("2024");
    }

    public String getPrice(){
        return priceText.getText();
    }

    public String getAirline(){
        return flightDetails.getText().split("|")[0];
    }

    public String getFlightClass(){
        return flightDetails.getText().split("|")[1];
    }

    public String[] getFlightDate(){
        return flightInfo.getText().split("|")[0].split(" ");
    }

    public String getDestinations(){
        return destinationText.getText();
    }

    public void submitDataButtonClick() { dataSubmitButton.click(); }

    public void nextSubmitDataButtonClick(WebDriver driver) {
        nextSubmitButton.click();
        WebDriverWait wait = new WebDriverWait(driver, 200);
        wait.until(ExpectedConditions.invisibilityOf(loaderText));
    }

    public void lookUpElement(WebElement element) {
        withAction().moveToElement(element);
    }
}
