package com.blibli.future.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static java.lang.Math.ceil;

@DefaultUrl("https://www.blibli.com/travel/tiket-pesawat")
public class FlightLandingPage extends PageObject {
    @FindBy(xpath="//p[contains(text(),'Mengapa pesan tiket pesawat di Blibli.com?')]")
    WebElement headerText;

    @FindBy(xpath="//button[contains(text(),'Cari Tiket')]")
    WebElement searchButton;

    @FindBy(xpath="//input[@placeholder='Pilih kota asal']")
    WebElement departureAirport;

    @FindBy(xpath="//input[@placeholder='Pilih kota tujuan']")
    WebElement arrivalAirport;

    @FindBy(xpath="//button[@class='button button--switch']")
    WebElement swapAirportButton;

    @FindBy(xpath="//ul[@class='FilteredAirports']")
    List<WebElement> filteredAirports;

    @FindBy(xpath="//div[contains(text(),'Pencarian tidak ditemukan')]")
    WebElement notFoundAirportLabel;

    @FindBy(xpath="//li[@class='list__items lendir popover__parent']//i[@class='icon bli-calendar-ic font-18']")
    WebElement departDateField;

    @FindBy(xpath="//td[contains(@class,'is-today')]")
    WebElement today;

    @FindBy(xpath="//button[@class='pika-next']")
    WebElement nextMonth;

    @FindBy(xpath="//li[contains(@class,'list__items popover__parent')]")
    WebElement roundtripReturnDateField;

    @FindBy(xpath="//td[contains(@class,'is-startrange')]")
    List<WebElement> dateStartrange;

    @FindBy(xpath="//td[contains(@class,'is-endrange')]")
    WebElement returnDate;

    @FindBy(xpath="//li[@class='list__items list__toggle grid__row margin-top-10']//div[1]")
    WebElement singletripRadioButton;

    @FindBy(xpath="//li[@class='list__items list__toggle grid__row margin-top-10']//div[2]")
    WebElement roundtripRadioButton;

    By adultDropdown = By.xpath("//li[1]//div[1]//select[1]");

    By teenDropdown = By.xpath("//li[2]//div[1]//select[1]");

    By infantDropdown = By.xpath("//li[3]//div[1]//select[1]");

    By flightClassDropdown = By.xpath("//select[@class='classType']");

    public String getHeaderText() {
        return headerText.getText();
    }

    public void searchTicket() {
        searchButton.click();
    }

    public void fillAirports(String departure, String arrival){
        departureAirport.sendKeys(departure + "\n");
        arrivalAirport.sendKeys(arrival + "\n");
    }

    public boolean fillDomesticAirportsNotPresent(String type, String location){
        if (type.equalsIgnoreCase("depart")) departureAirport.sendKeys(location);
        else arrivalAirport.sendKeys(location);
        return notFoundAirportLabel.findElement(By.xpath("..")).isDisplayed();
    }

    public boolean searchAirportsAutoComplete(String type, String location){
        boolean suggestionsAreRelevant = false;
        if (type.equalsIgnoreCase("depart")) departureAirport.sendKeys(location);
        else arrivalAirport.sendKeys(location);
        for (WebElement element : filteredAirports){
            if (!element.isDisplayed()) filteredAirports.remove(element);
            suggestionsAreRelevant = element.findElement(By.xpath(".//*")).findElement(By.xpath(".//small")).getText().contains(location);
        }
        return suggestionsAreRelevant;
    }

    public boolean disabledCalendarDayBeforeToday(){
        departDateField.click();
        withAction().moveToElement(today);
        int yesterday = Integer.parseInt(today.findElement(By.xpath(".//button")).getAttribute("data-pika-day")) - 1;
        int month = Integer.parseInt(today.findElement(By.xpath(".//button")).getAttribute("data-pika-month"));
        return !today.
                findElement(
                        By.xpath(
                                "//button[@data-pika-month='"+month+
                                        "' and @data-pika-day='"
                                        +yesterday+"']"))
                .isEnabled();
    }

    public void chooseDateOfFlight(WebDriver driver, int date, int month, int year){
        WebDriverWait wait = new WebDriverWait(driver, 35);
        wait.until(ExpectedConditions.elementToBeClickable(departDateField)).click();
        withAction().moveToElement(today);

        int thisMonth = Integer.parseInt(wait.until(ExpectedConditions.visibilityOf(today.findElement(By.xpath(".//button")))).getAttribute("data-pika-month"));
        int thisYear = Integer.parseInt(wait.until(ExpectedConditions.visibilityOf(today.findElement(By.xpath(".//button")))).getAttribute("data-pika-year"));
        if ((month > thisMonth) && (year-thisYear <= 1)){
            for (int i = ((int) ceil((month-thisMonth)/2))+1; i > 0; i--){
                wait.until(ExpectedConditions.elementToBeClickable(nextMonth)).click();
            }
        }
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@data-pika-month='"+month+"' and @data-pika-day='"+date+"']"))).click();
    }

    public boolean dateFieldDisabledIfNotRoundtrip(){
        return singletripRadioButton.isSelected() && !roundtripReturnDateField.isEnabled();
    }

    public boolean cannotReturnBeforeDepartDate(WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        clickRoundtripRadioButton(driver);
        wait.until(ExpectedConditions.elementToBeClickable(roundtripReturnDateField)).click();
        withAction().moveToElement(returnDate);
        int departMonth = Integer.parseInt(wait.until(ExpectedConditions.visibilityOf(dateStartrange.stream().findFirst().get().findElement(By.xpath(".//button")))).getAttribute("data-pika-month"));
        int departDate = Integer.parseInt(wait.until(ExpectedConditions.visibilityOf(dateStartrange.stream().findFirst().get().findElement(By.xpath(".//button")))).getAttribute("data-pika-day"));
        int returnMonthEndrange = Integer.parseInt(wait.until(ExpectedConditions.visibilityOf(returnDate.findElement(By.xpath(".//button")))).getAttribute("data-pika-month"));
        int returnDateEndrange = Integer.parseInt(wait.until(ExpectedConditions.visibilityOf(returnDate.findElement(By.xpath(".//button")))).getAttribute("data-pika-day"));
        int returnMonthStartrange = Integer.parseInt(wait.until(ExpectedConditions.visibilityOf(dateStartrange.get(dateStartrange.size()-1).findElement(By.xpath(".//button")))).getAttribute("data-pika-month"));
        int returnDateStartrange = Integer.parseInt(wait.until(ExpectedConditions.visibilityOf(dateStartrange.get(dateStartrange.size()-1).findElement(By.xpath(".//button")))).getAttribute("data-pika-day"));
        return returnMonthEndrange >= departMonth
                && returnDateEndrange >= departDate
                && returnMonthStartrange >= departMonth
                && returnDateStartrange >= departDate;
    }

    public void clickSingletripRadioButton(WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(singletripRadioButton)).click();
    }

    public void clickRoundtripRadioButton(WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(roundtripRadioButton)).click();
    }

    public void chooseReturnDateRoundtrip(WebDriver driver, int date, int month, int year){
        WebDriverWait wait = new WebDriverWait(driver, 35);
        wait.until(ExpectedConditions.elementToBeClickable(roundtripRadioButton)).click();
        wait.until(ExpectedConditions.elementToBeClickable(roundtripReturnDateField)).click();
        withAction().moveToElement(returnDate);
        int thisMonth = Integer.parseInt(wait.until(ExpectedConditions.visibilityOf(returnDate.findElement(By.xpath(".//button")))).getAttribute("data-pika-month"));
        int thisYear = Integer.parseInt(wait.until(ExpectedConditions.visibilityOf(returnDate.findElement(By.xpath(".//button")))).getAttribute("data-pika-year"));
        if ((month > thisMonth) && (year-thisYear <= 1)){
            for (int i = ((int) ceil((month-thisMonth)/2)); i > 0; i--){
                if (!nextMonth.isDisplayed()) {
                    nextMonth = driver.findElements(By.xpath("//button[@class='pika-next']")).stream().filter(WebElement::isDisplayed).findFirst().get();
                }
                wait.until(ExpectedConditions.elementToBeClickable(nextMonth)).click();
            }
        }
        WebElement day = driver.findElement(By.xpath("//button[@data-pika-month='"+month+"' and @data-pika-day='"+date+"']"));
        if (!day.isDisplayed()) {
            day = driver.findElements(By.xpath("//button[@data-pika-month='"+month+"' and @data-pika-day='"+date+"']")).stream().filter(WebElement::isDisplayed).findFirst().get();
        }
        wait.until(ExpectedConditions.elementToBeClickable(day)).click();
    }

    public void fillPassengerTotal(WebDriver driver, int adult, int teen, int infant){
        Select selectAdultDropdown = new Select(driver.findElement(adultDropdown));
        selectAdultDropdown.selectByVisibleText(String.valueOf(adult));

        Select selectTeenDropdown = new Select(driver.findElement(teenDropdown));
        selectTeenDropdown.selectByVisibleText(String.valueOf(teen));

        Select selectInfantDropdown = new Select(driver.findElement(infantDropdown));
        selectInfantDropdown.selectByVisibleText(String.valueOf(infant));
    }

    public void fillFlightClassType(WebDriver driver, String type){
        Select selectClassDropdown = new Select(driver.findElement(flightClassDropdown));
        selectClassDropdown.selectByVisibleText(type);
    }

    public void clickSwapAirportsButton(WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver, 35);
        wait.until(ExpectedConditions.elementToBeClickable(swapAirportButton)).click();
    }
}
