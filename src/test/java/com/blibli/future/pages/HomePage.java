package com.blibli.future.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

@DefaultUrl("https://www.blibli.com")
public class HomePage extends PageObject {
    @FindBy(xpath="//div[contains(text(),'Pesawat')]")
    WebElement travelIcon;

    @FindBy(xpath="//div[@class='favourite__header']//a[contains(text(),'Lihat Semua')]")
    WebElement lihatSemuaFavoritHyperlink;

    @FindBy(xpath="//a[contains(text(),'Travel')]")
    WebElement travelTabFavoriteHyperlink;

    @FindBy(xpath="//div[@class='blu-columns b-mobile b-multiline']//div[1]//a[1]//div[1]//img[1]")
    WebElement iconPesawatImage;

    public void openFlightDirectlyFromTravelIconOnHomePage() {
        travelIcon.click();
    }

    public void openFlightFromFavorites() {
        lihatSemuaFavoritHyperlink.click();

        travelTabFavoriteHyperlink.click();

        iconPesawatImage.click();
    }
}
