package com.blibli.future.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class FlightSearchPage extends PageObject {
    @FindBy(xpath="//h4[contains(text(),'Pencarian Pergi')]")
    WebElement textContainer;

    @FindBy(xpath="//*[contains(text(),'Pilih Pergi')]")
    List<WebElement> ticketList;

    @FindBy(xpath="//div[@class='route__departure-city']")
    WebElement departureAirportText;

    @FindBy(xpath="//div[@class='route__arrival-city']")
    WebElement arrivalAirportText;

    @FindBy(xpath="//span[@class='search__info-data-date']")
    List<WebElement> departAndArriveDate;

    @FindBy(xpath="//span[@class='search__info-data-passanger']")
    WebElement passengersInfoText;

    @FindBy(xpath="//span[@class='search__info-data-class']")
    WebElement flightClassText;

    @FindBy(xpath="//label[contains(text(),'Pagi (00:00 - 12:00)')]")
    WebElement morningFlightCheckbox;

    @FindBy(xpath="//label[contains(text(),'Siang (12:00 - 18:00)')]")
    WebElement noonFlightCheckbox;

    @FindBy(xpath="//label[contains(text(),'Malam (18:00 - 24:00)')]")
    WebElement nightFlightCheckbox;

    @FindBy(xpath="//div[@class='noUi-handle noUi-handle-upper']")
    WebElement priceUpperSlider;

    @FindBy(xpath="//div[@class='noUi-handle noUi-handle-lower']")
    WebElement priceLowerSlider;

    @FindBy(xpath="//ul[@class='list__group']//li[contains(text(),'Rp')]")
    WebElement priceRangeText;

    @FindBy(xpath="//label[contains(text(),'Tanpa Transit')]")
    WebElement noTransitCheckbox;

    @FindBy(xpath="//label[contains(text(),'1 transit')]")
    WebElement oneTransitCheckbox;

    @FindBy(xpath="//label[contains(text(),'2+ transit')]")
    WebElement twoOrMoreTransitsCheckbox;

    @FindBy(xpath="//span[@class='catalog__filter-header'][contains(text(),'Berangkat')]")
    WebElement departTimeSort;

    @FindBy(xpath="//span[@class='catalog__filter-header'][contains(text(),'Tiba')]")
    WebElement arriveTimeSort;

    @FindBy(xpath="//span[@class='catalog__filter-header'][contains(text(),'Durasi')]")
    WebElement durationSort;

    @FindBy(xpath="//span[@class='catalog__filter-header'][contains(text(),'Harga')]")
    WebElement priceSort;

    @FindBy(xpath="//a[@id='sort-departure']//span[@class='sort__down']")
    WebElement sortDepartureTimeDescending;

    @FindBy(xpath="//a[@id='sort-departure']//span[@class='sort__up']")
    WebElement sortDepartureTimeAscending;

    @FindBy(xpath="//a[@id='sort-price']//span[@class='sort__down']")
    WebElement sortPriceAscending;

    @FindBy(xpath="//a[@id='sort-price']//span[@class='sort__up']")
    WebElement sortPriceDescending;

    @FindBy(xpath="//a[@id='sort-arrival']//span[@class='sort__down']")
    WebElement sortArriveTimeDescending;

    @FindBy(xpath="//a[@id='sort-arrival']//span[@class='sort__up']")
    WebElement sortArriveTimeAscending;

    @FindBy(xpath="//a[@id='sort-duration']//span[@class='sort__down']")
    WebElement sortDurationDescending;

    @FindBy(xpath="//a[@id='sort-duration']//span[@class='sort__up']")
    WebElement sortDurationAscending;

    @FindBy(xpath="//div[@class='list__togel-content']")
    List<WebElement> listItems;

    public void sortByDepartureTime(WebDriver driver, String type){
        WebDriverWait wait = new WebDriverWait(driver, 35);
        if (type.equalsIgnoreCase("ascending")){
            if (!sortDepartureTimeDescending.isDisplayed() && sortDepartureTimeAscending.isDisplayed()) return;
            wait.until(ExpectedConditions.elementToBeClickable(departTimeSort)).click();
        } else if (type.equalsIgnoreCase("descending")){
            if (sortDepartureTimeDescending.isDisplayed() && sortDepartureTimeAscending.isDisplayed()) {
                wait.until(ExpectedConditions.elementToBeClickable(departTimeSort)).click();
                wait.until(ExpectedConditions.elementToBeClickable(departTimeSort)).click();
            } else if (!sortDepartureTimeDescending.isDisplayed() && sortDepartureTimeAscending.isDisplayed()){
                wait.until(ExpectedConditions.elementToBeClickable(departTimeSort)).click();
            }
        }
    }

    public void sortByArriveTime(WebDriver driver, String type){
        WebDriverWait wait = new WebDriverWait(driver, 35);
        if (type.equalsIgnoreCase("ascending")){
            if (!sortArriveTimeDescending.isDisplayed() && sortArriveTimeAscending.isDisplayed()) return;
            wait.until(ExpectedConditions.elementToBeClickable(arriveTimeSort)).click();
        } else if (type.equalsIgnoreCase("descending")){
            if (sortArriveTimeDescending.isDisplayed() && sortArriveTimeAscending.isDisplayed()) {
                wait.until(ExpectedConditions.elementToBeClickable(arriveTimeSort)).click();
                wait.until(ExpectedConditions.elementToBeClickable(arriveTimeSort)).click();
            } else if (!sortArriveTimeDescending.isDisplayed() && sortArriveTimeAscending.isDisplayed()){
                wait.until(ExpectedConditions.elementToBeClickable(arriveTimeSort)).click();
            }
        }
    }

    public void sortByDuration(WebDriver driver, String type){
        WebDriverWait wait = new WebDriverWait(driver, 35);
        if (type.equalsIgnoreCase("ascending")){
            if (!sortDurationDescending.isDisplayed() && sortDurationAscending.isDisplayed()) return;
            wait.until(ExpectedConditions.elementToBeClickable(durationSort)).click();
        } else if (type.equalsIgnoreCase("descending")){
            if (sortDurationDescending.isDisplayed() && sortDurationAscending.isDisplayed()) {
                wait.until(ExpectedConditions.elementToBeClickable(durationSort)).click();
                wait.until(ExpectedConditions.elementToBeClickable(durationSort)).click();
            } else if (!sortDurationDescending.isDisplayed() && sortDurationAscending.isDisplayed()){
                wait.until(ExpectedConditions.elementToBeClickable(durationSort)).click();
            }
        }
    }

    public void sortByPrice(WebDriver driver, String type){
        WebDriverWait wait = new WebDriverWait(driver, 35);
        if (type.equalsIgnoreCase("ascending")){
            if (!sortPriceDescending.isDisplayed() && sortPriceAscending.isDisplayed()) return;
            wait.until(ExpectedConditions.elementToBeClickable(priceSort)).click();
        } else if (type.equalsIgnoreCase("descending")){
            if (sortPriceDescending.isDisplayed() && sortPriceAscending.isDisplayed()) {
                wait.until(ExpectedConditions.elementToBeClickable(priceSort)).click();
                wait.until(ExpectedConditions.elementToBeClickable(priceSort)).click();
            } else if (!sortPriceDescending.isDisplayed() && sortPriceAscending.isDisplayed()){
                wait.until(ExpectedConditions.elementToBeClickable(priceSort)).click();
            }
        }
    }

    public void clickMorningFlightCheckbox(WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver, 35);
        wait.until(ExpectedConditions.elementToBeClickable(morningFlightCheckbox)).click();
    }

    public void clickNoonFlightCheckbox(WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver, 35);
        wait.until(ExpectedConditions.elementToBeClickable(noonFlightCheckbox)).click();
    }

    public void clickNightFlightCheckbox(WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver, 35);
        wait.until(ExpectedConditions.elementToBeClickable(nightFlightCheckbox)).click();
    }

    public void clickNoTransitCheckbox(WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver, 35);
        wait.until(ExpectedConditions.elementToBeClickable(noTransitCheckbox)).click();
    }

    public void clickOneTransitCheckbox(WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver, 35);
        wait.until(ExpectedConditions.elementToBeClickable(oneTransitCheckbox)).click();
    }

    public void clickTwoOrMoreTransitCheckbox(WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver, 35);
        wait.until(ExpectedConditions.elementToBeClickable(twoOrMoreTransitsCheckbox)).click();
    }

    public String trySearchForFirstOccurrenceAirlineFilter(WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver, 35);
        WebElement airline = listItems.stream().findFirst().get();
        String airlineName = airline.getText();
        wait.until(ExpectedConditions.elementToBeClickable(airline)).click();
        return airlineName;
    }

    public int priceSliderFilterToMax(WebDriver driver){
        Dimension sliderSize = priceUpperSlider.getSize();
        int sliderWidth = sliderSize.getWidth();

        int xCoord = priceUpperSlider.getLocation().getX();

        Actions builder = new Actions(driver);
        builder.moveToElement(priceLowerSlider)
                .click()
                .dragAndDropBy
                        (priceLowerSlider,xCoord + sliderWidth, 0)
                .build()
                .perform();

        return getPriceFilter(0);
    }

    public int getPriceFilter(int index){
        String[] prices = priceRangeText.getText().split(" - ");
        return Integer.parseInt(prices[index].substring(2).replace(".",""));
    }

    public String getDepartureAirportText() {
        return departureAirportText.getText();
    }

    public String getArrivalAirportText() {
        return arrivalAirportText.getText();
    }

    public List<WebElement> getDepartAndArriveDate() {
        return departAndArriveDate;
    }

    public String getPassengersInfoText() {
        return passengersInfoText.getText();
    }

    public String getFlightClassText() {
        return flightClassText.getText();
    }

    public String getTextContainer() {
        return textContainer.getText();
    }

    public void pickFirstOccurrenceTicket(){
        ticketList.stream().findFirst().get().click();
    }

}
